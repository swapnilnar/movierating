package com.android.movieratings.sort

import com.android.movieratings.model.Movie
import org.junit.Assert.*
import org.junit.Test

class SortMoviesTest{
    private val sortItems:SortMovies = SortMovies()

    private val movies = listOf(
        Movie("The Shawshank Redemption", 5.0f),
        Movie("The Godfather", 5.0f),
        Movie("The Dark Knight", 4.0f),
        Movie("The Godfather: Part II", 3.5f),
        Movie("The Lord of the Rings: The Return of the King", 4.2f),
        Movie("Pulp Fiction", 4.1f),
        Movie("Schindler's List", 2.5f),
        Movie("12 Angry Men", 1.5f),
        Movie("Fight Club", 1f),
        Movie("Forrest Gump", 2.5f)
    )

    @Test
    fun itShouldReturnSortedList() {
        val sortedList = sortItems.sortByRating(movies)

        val expectedList = movies.sortedByDescending { it.rating }

        assertEquals(expectedList, sortedList)
    }
}