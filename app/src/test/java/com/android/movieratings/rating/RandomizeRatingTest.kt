package com.android.movieratings.rating

import com.android.movieratings.di.Scheduler
import com.android.movieratings.model.Movie
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.*
import org.junit.Test

class RandomizeRatingTest{
    private val scheduler = TestScheduler()
    private val ratingSystem = RandomizeRating(scheduler)

    private val movies = listOf(
        Movie("The Shawshank Redemption", 5.0f)
    )

    private val testObserver = TestObserver<List<Movie>>()

    @Test
    fun itShouldReturnRandomListOfMoviesWithRandomRatings() {
        val initialRating = movies[0].rating
        var resultMovieRating = 0f
        val observable = ratingSystem.randomizeRating(movies)

        observable.subscribe(testObserver)

        val disposable = observable.subscribe {
            resultMovieRating = it[0].rating
        }

        testObserver.hasSubscription()

        testObserver.awaitCount(1)
        testObserver.assertValueCount(1)
        assertNotEquals(initialRating, resultMovieRating)
        testObserver.assertNotComplete()
        disposable.dispose()
    }

    class TestScheduler : Scheduler {
        override fun mainThread(): io.reactivex.Scheduler {
            return Schedulers.trampoline()
        }

        override fun io(): io.reactivex.Scheduler {
            return Schedulers.trampoline()
        }

    }
}