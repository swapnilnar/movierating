package com.android.movieratings.model

import com.android.movieratings.persist.MovieDataRepository
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.observers.TestObserver
import org.junit.Test

class ListViewModelTest{
    private val repository: MovieDataRepository  = mock()
    private val listViewModel = ListViewModel(repository)
    var observer: TestObserver<List<Movie>> = TestObserver()
    private val movies = listOf(
        Movie("The Shawshank Redemption", 5.0f),
        Movie("The Godfather", 5.0f),
        Movie("The Dark Knight", 4.0f),
        Movie("The Godfather: Part II", 3.5f),
        Movie("The Lord of the Rings: The Return of the King", 4.2f),
        Movie("Pulp Fiction", 4.1f),
        Movie("Schindler's List", 2.5f),
        Movie("12 Angry Men", 1.5f),
        Movie("Fight Club", 1f),
        Movie("Forrest Gump", 2.5f)
    )

    @Test
    fun itShouldReturnListOfData() {
        val observable = listViewModel.fetchMovies()

        observable.subscribe(observer)

        observer.assertSubscribed()

        observer.assertResult(movies)

        observer.assertComplete()
    }
}