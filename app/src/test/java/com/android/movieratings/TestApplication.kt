package com.android.movieratings

import com.android.movieratings.di.AppModuleTest
import com.android.movieratings.di.DaggerTestApplicationComponent
import com.android.movieratings.di.TestApplicationComponent

class TestApplication: App() {
    companion object {
        lateinit var appComponent : TestApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    private fun initDi() {
        appComponent = DaggerTestApplicationComponent.builder().appModuleTest(AppModuleTest(this)).build()
    }
}