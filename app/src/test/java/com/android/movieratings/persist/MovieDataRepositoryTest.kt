package com.android.movieratings.persist

import org.junit.Assert.*
import org.junit.Test

class MovieDataRepositoryTest{
    private val repository = MovieDataRepository()

    @Test
    fun itShouldReturnListOfTenMovies() {
        assertEquals(10, repository.getTopTenMovies().size)
    }
}