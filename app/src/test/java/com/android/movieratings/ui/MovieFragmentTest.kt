package com.android.movieratings.ui

import android.widget.RatingBar
import com.android.movieratings.R
import com.android.movieratings.TestApplication
import kotlinx.android.synthetic.main.fragment_movie.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [23],application = TestApplication::class)
class MovieFragmentTest {

    private val activity = Robolectric.setupActivity(MovieActivity::class.java)

    private lateinit var fragment: MovieFragment

    @Before
    fun setUp() {
        TestApplication.appComponent.inject(this)
        this.fragment = this.activity.supportFragmentManager.findFragmentByTag("movies") as MovieFragment
    }

    @Test
    fun fragmentShouldNotNull() {
        assertNotNull(fragment)
    }

    @Test
    fun itShouldHaveHasOptionMenu() {
        assertTrue(fragment.hasOptionsMenu())
    }

    @Test
    fun itShouldNotNullOnRatingChangeListener() {
        assertNotNull(fragment.adapter.listener)
    }

    @Test
    fun itShouldHaveSetAdapterToRecyclerView() {
        assertNotNull(fragment.rvMovies.adapter)
    }

    @Test
    fun itShouldHaveItemsInRecyclerView() {
        assertNotNull(fragment.rvMovies.adapter)
        assertEquals(10, fragment.rvMovies.adapter?.itemCount)
    }

    @Test
    fun itShouldSortListOnRatingChange() {
        val firstItem = fragment.rvMovies.layoutManager?.findViewByPosition(0)
        assertNotNull(firstItem)
        val rating = firstItem?.findViewById<RatingBar>(com.android.movieratings.R.id.rating)
        assertEquals(5, rating?.numStars)
        assertEquals(5f, rating?.rating)
        rating?.rating = 4f
        val latestFirstItem = fragment.rvMovies.layoutManager?.findViewByPosition(0)
        assertNotEquals(firstItem, latestFirstItem)
    }

    @Test
    fun itShouldMatchMenuItemTitle() {
        val activity = Robolectric.setupActivity(MovieActivity::class.java)
        val menu = shadowOf(activity).optionsMenu
        assertEquals("random", menu.findItem(R.id.action_random).title)
    }
}