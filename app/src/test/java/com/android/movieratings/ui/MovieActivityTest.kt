package com.android.movieratings.ui

import com.android.movieratings.TestApplication
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [23],application = TestApplication::class)
class MovieActivityTest{
    private val activity = Robolectric.buildActivity(MovieActivity::class.java).create().start().resume().visible().get()


    @Test
    fun itShouldNotNull() {
        assertNotNull(activity)
    }

    @Test
    fun itShouldHaveMovieFragmentAsVisibleFragment(){
        val fragment = (activity.supportFragmentManager.findFragmentByTag("movies") as MovieFragment)
        assertEquals(MovieFragment::class.simpleName, fragment::class.simpleName)
        assertTrue(fragment.isVisible)
        assertTrue(fragment.isAdded)
    }

}