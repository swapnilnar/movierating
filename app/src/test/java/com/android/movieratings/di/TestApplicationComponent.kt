package com.android.movieratings.di

import com.android.movieratings.di.AppComponent
import com.android.movieratings.di.AppModule
import com.android.movieratings.di.AppModuleTest
import com.android.movieratings.ui.MovieFragmentTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModuleTest::class])
interface TestApplicationComponent : AppComponent {
    fun inject(fragment: MovieFragmentTest)
}