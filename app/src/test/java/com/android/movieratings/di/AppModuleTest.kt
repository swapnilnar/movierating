package com.android.movieratings.di

import android.content.Context
import com.android.movieratings.rating.RandomizeRating
import com.android.movieratings.factory.ListViewModelFactory
import com.android.movieratings.persist.MovieDataRepository
import com.android.movieratings.sort.SortMovies
import com.android.movieratings.ui.adapter.MovieListAdapter
import com.android.movieratings.ui.provider.FragmentProvider
import com.android.movieratings.ui.provider.FragmentProviderImp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModuleTest(private val context: Context) {

    @Provides
    @Singleton
    fun providesContext(): Context = context

    @Provides
    fun adapter(): MovieListAdapter = MovieListAdapter()

    @Provides
    @Singleton
    fun movieRepository(): MovieDataRepository = MovieDataRepository()

    @Provides
    fun listViewModelFactory(repository: MovieDataRepository): ListViewModelFactory = ListViewModelFactory(repository)

    @Provides
    @Singleton
    fun sortItems() : SortMovies = SortMovies()

    @Provides
    @Singleton
    fun scheduler(): Scheduler = AppScheduler()

    @Provides
    @Singleton
    fun ratingSystem(scheduler: Scheduler) : RandomizeRating =
        RandomizeRating(scheduler)

    @Provides
    @Singleton
    fun getFragmentProvider() : FragmentProvider = FragmentProviderImp()
}