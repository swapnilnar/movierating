package com.android.movieratings.model

import android.arch.lifecycle.ViewModel
import com.android.movieratings.persist.MovieDataRepository
import io.reactivex.Observable

class ListViewModel(private val repo: MovieDataRepository) : ViewModel(){
    fun fetchMovies() : Observable<List<Movie>> {
        return Observable.just(repo.getTopTenMovies())
    }
}