package com.android.movieratings.model

data class Movie(val name: String, var rating: Float = 5.0f)