package com.android.movieratings.di

import android.content.Context
import com.android.movieratings.rating.RandomizeRating
import com.android.movieratings.persist.MovieDataRepository
import com.android.movieratings.sort.SortMovies
import com.android.movieratings.ui.MovieActivity
import com.android.movieratings.ui.MovieFragment
import com.android.movieratings.ui.adapter.MovieListAdapter
import com.android.movieratings.ui.provider.FragmentProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun context() : Context

    fun adapter() : MovieListAdapter

    fun movieRepository() : MovieDataRepository

    fun sortMovies() : SortMovies

    fun scheduler() : Scheduler

    fun randomRating() : RandomizeRating

    fun getFragmentProvider() : FragmentProvider

    fun inject(fragment: MovieFragment)

    fun inject(activity: MovieActivity)
}