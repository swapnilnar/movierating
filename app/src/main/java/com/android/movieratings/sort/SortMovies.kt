package com.android.movieratings.sort

import com.android.movieratings.model.Movie

class SortMovies {
    fun sortByRating(list: List<Movie>): List<Movie> {
        return list.sortedByDescending { it.rating }
    }
}