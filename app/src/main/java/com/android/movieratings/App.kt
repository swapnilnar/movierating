package com.android.movieratings

import android.app.Application
import com.android.movieratings.di.AppComponent
import com.android.movieratings.di.AppModule
import com.android.movieratings.di.DaggerAppComponent

open class App : Application() {
    companion object {
        lateinit var appComponent : AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    private fun initDi() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}