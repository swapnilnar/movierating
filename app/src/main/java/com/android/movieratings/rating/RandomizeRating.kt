package com.android.movieratings.rating

import com.android.movieratings.di.Scheduler
import com.android.movieratings.model.Movie
import io.reactivex.Observable
import java.util.*
import java.util.concurrent.TimeUnit

class RandomizeRating(private val scheduler: Scheduler) {
    fun randomizeRating(list:List<Movie>) : Observable<List<Movie>> {
        return Observable.interval(500, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.mainThread())
            .subscribeOn(scheduler.io())
            .map { list.random().rating = getRandomRating(0f, 5f)
                list }
    }

    private fun getRandomRating(min:Float, max:Float) : Float {
        val random = Random()
        return random.nextFloat() * (max - min) + min
    }
}