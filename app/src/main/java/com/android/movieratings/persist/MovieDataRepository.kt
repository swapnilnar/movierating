package com.android.movieratings.persist

import com.android.movieratings.model.Movie

open class MovieDataRepository {
    fun getTopTenMovies(): List<Movie> {
        return listOf(
            Movie("The Shawshank Redemption", 5.0f),
            Movie("The Godfather", 5.0f),
            Movie("The Dark Knight", 4.0f),
            Movie("The Godfather: Part II", 3.5f),
            Movie("The Lord of the Rings: The Return of the King", 4.2f),
            Movie("Pulp Fiction", 4.1f),
            Movie("Schindler's List", 2.5f),
            Movie("12 Angry Men", 1.5f),
            Movie("Fight Club", 1f),
            Movie("Forrest Gump", 2.5f)
        )
    }
}