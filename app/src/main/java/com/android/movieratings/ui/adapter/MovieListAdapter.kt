package com.android.movieratings.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.movieratings.R
import com.android.movieratings.model.Movie
import com.android.movieratings.ui.listener.RatingChangeListener
import kotlinx.android.synthetic.main.movie_item.view.*

class MovieListAdapter : RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>() {

    private val movies = arrayListOf<Movie>()

    var listener: RatingChangeListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    fun setData(movies: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.itemView.rating.onRatingBarChangeListener = null
        holder.bind(movie)
        holder.itemView.rating.setOnRatingBarChangeListener{_,rating,_->
            movie.rating = rating
            listener?.ratingChanged(movies)
        }

    }

    class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movie: Movie) {
            itemView.title.text = movie.name
            itemView.rating.rating = movie.rating
        }
    }
}