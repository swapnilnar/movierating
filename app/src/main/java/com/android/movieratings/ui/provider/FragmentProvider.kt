package com.android.movieratings.ui.provider

import android.support.v4.app.Fragment
import com.android.movieratings.ui.MovieFragment

interface FragmentProvider {
    fun getMoviesFragment() : Fragment {
        return MovieFragment()
    }
}

class FragmentProviderImp: FragmentProvider