package com.android.movieratings.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.android.movieratings.App
import com.android.movieratings.R
import com.android.movieratings.rating.RandomizeRating
import com.android.movieratings.model.ListViewModel
import com.android.movieratings.factory.ListViewModelFactory
import com.android.movieratings.model.Movie
import com.android.movieratings.sort.SortMovies
import com.android.movieratings.ui.adapter.MovieListAdapter
import com.android.movieratings.ui.listener.RatingChangeListener
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_movie.*
import javax.inject.Inject

class MovieFragment : Fragment(), RatingChangeListener {

    @Inject
    lateinit var adapter: MovieListAdapter

    @Inject
    lateinit var viewModelFactory: ListViewModelFactory

    @Inject
    lateinit var sortMovies: SortMovies

    @Inject
    lateinit var randomRating: RandomizeRating

    private val movies: ArrayList<Movie> = arrayListOf()

    private var disposable: Disposable? = null

    private val viewModel: ListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvMovies.adapter = adapter
        adapter.listener = this
        disposable = viewModel.fetchMovies().subscribe { movies ->
            this.movies.addAll(movies)
            adapter.setData(sortMovies.sortByRating(movies))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_random -> {
                randomizeList(item)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private var randomRatingDisposable: Disposable? = null

    private fun randomizeList(item: MenuItem) {
        randomRatingDisposable = randomRatingDisposable.let {
            if (it?.isDisposed == null) randomRating.randomizeRating(movies).subscribe { randomMovies ->
                adapter.setData(sortMovies.sortByRating(randomMovies))
                item.title = getString(R.string.action_stop)
            } else {
                it.dispose()
                item.title = getString(R.string.action_random)
                null
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (disposable?.isDisposed == false) disposable?.dispose()
    }

    override fun ratingChanged(movies: List<Movie>) {
        adapter.setData(sortMovies.sortByRating(movies))
    }
}