package com.android.movieratings.ui.listener

import com.android.movieratings.model.Movie

interface RatingChangeListener {
    fun ratingChanged(movies:List<Movie>)
}